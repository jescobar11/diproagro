<?php $agrodir_option_data =get_option('agrodir_option_data');  ?>



<?php 

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

    if ( is_plugin_active('sitepress-multilingual-cms/sitepress.php') && $agrodir_option_data['agrodir-top-language'] == 1){
    	
    	agrodir_wpml_languages();

    } else {  ?>


		<?php if(isset($agrodir_option_data['agrodir-top-language']) && $agrodir_option_data['agrodir-top-language'] == 1) : ?>
		<div class="language">
			<?php if(isset($agrodir_option_data['agrodir-language']) && is_array($agrodir_option_data['agrodir-language']) && !empty($agrodir_option_data['agrodir-language'])) : ?>
			<a class = "toggle" href = "" >
				EN
			</a>

				<ul>
					<?php foreach($agrodir_option_data['agrodir-language'] as $key => $value){ ?>

					<li><a href="#"><?php echo esc_attr($value); ?></a></li>
					
					<?php } ?>
				</ul>

			<?php endif; ?>
		</div>
		<?php endif; ?>

<?php } ?>
<!-- End Header-Language -->