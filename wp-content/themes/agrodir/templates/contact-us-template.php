<?php
/**
 * Template Name: Contact Us Template
 *
 */
 ?>
<?php get_header(); ?>

   <?php
		$top_breadcrumb_image= agrodir_IMAGE."banner-breadcrumb.jpg";
        if(isset($agrodir_option_data['agrodir-banner-breadcrumb']['url']) AND $agrodir_option_data['agrodir-banner-breadcrumb']['url']!=""):
			$top_breadcrumb_image=esc_url($agrodir_option_data['agrodir-banner-breadcrumb']['url']);
         endif;
         
         $agrodir_breadcrumb_value='1';
         if(isset($agrodir_option_data['agrodir-breadcrumb']) AND $agrodir_option_data['agrodir-breadcrumb']!=""):
			$agrodir_breadcrumb_value=$agrodir_option_data['agrodir-breadcrumb'];
         endif;
         
         
         if($agrodir_breadcrumb_value=='1'){ 
		?>
		 <div class="breadcrumb-content">
			<img   src="<?php echo $top_breadcrumb_image;?>" alt="<?php esc_html_e( 'banner', 'agrodir' ); ?>">
			<div class="container">
				<h3> <?php
					 the_title();
					?></h3>
			</div>
		</div>	
		<?php
			}
		?>
  <div class="blog-content">


          <?php echo apply_filters('the_content',$post->post_content); ?>


  </div> <!--  end blog-single -->


<?php get_footer();
