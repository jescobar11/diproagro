    <div class="box-shadow-for-ui">
      <div class="uou-block-2d">
        <div class="container">
			<?php $agrodir_option_data =get_option('agrodir_option_data'); ?>
			<?php
        $top_logo_image= agrodir_IMAGE."agrodir-logo.png";
        if(isset($agrodir_option_data['agrodir-header-icon']['url']) AND $agrodir_option_data['agrodir-header-icon']['url']!=""):
			$top_logo_image=esc_url($agrodir_option_data['agrodir-header-icon']['url']);
         endif; ?>
         
          <a href="<?php echo esc_url(site_url('/')); ?>" class="logo">  <img src="<?php echo esc_attr($top_logo_image); ?>" alt="<?php esc_html_e( 'image', 'agrodir' ); ?>"> </a>
          <a href="#" class="mobile-sidebar-button mobile-sidebar-toggle"><span></span></a>
        </div>
      </div> <!-- end .uou-block-2d -->
    </div>
