    <div class="box-shadow-for-ui">
      <div class="uou-block-2a">
        <div class="container">
          <a href="<?php echo esc_url(site_url('/')); ?>" class="logo"> <img src="<?php echo esc_url(agrodir_IMAGE); ?>agrodir-logo.png" alt="<?php esc_html_e( 'image', 'agrodir' ); ?>"> </a>
          <a href="#" class="mobile-sidebar-button mobile-sidebar-toggle"><span></span></a>
        </div>
      </div> <!-- end .uou-block-2a -->
    </div>