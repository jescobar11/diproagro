    <div class="uou-block-1c">
	   <div class="container">
		   <?php
        $top_logo_image= agrodir_IMAGE."agrodir-logo.png";
        if(isset($agrodir_option_data['agrodir-header-icon']['url']) AND $agrodir_option_data['agrodir-header-icon']['url']!=""):
			$top_logo_image=esc_url($agrodir_option_data['agrodir-header-icon']['url']);
         endif; ?>
         
      <a href="<?php echo esc_url(site_url('/')); ?>" class="logo"> 
      <img src="<?php echo esc_attr($top_logo_image); ?>" alt="<?php esc_html_e( 'image', 'agrodir' ); ?>"> </a>

      <div class="search">
        <?php get_search_form(); ?>
      </div>
      	<?php get_template_part('templates/header','socialButton'); ?>  
    </div>
  </div> <!-- end .uou-block-1a -->

