    <div class="box-shadow-for-ui">
      <div class="uou-block-2d">
        <div class="container">
          <div class="contact">
			  <?php
        $top_logo_image= agrodir_IMAGE."agrodir-logo.png";
        if(isset($agrodir_option_data['agrodir-header-icon']['url']) AND $agrodir_option_data['agrodir-header-icon']['url']!=""):
			$top_logo_image=esc_url($agrodir_option_data['agrodir-header-icon']['url']);
         endif; ?>
         
         <?php $agrodir_option_data =get_option('agrodir_option_data'); ?>
            <?php
			  $agrodirtopphone='(02) 123-456-7890';
			 if(isset($agrodir_option_data['agrodir-top-phone']) AND $agrodir_option_data['agrodir-top-phone']!=""):
				$agrodirtopphone=$agrodir_option_data['agrodir-top-phone'];
			 endif;
         
			?>
			
            <span>Call Us:</span>
             <a href="tel:<?php echo $agrodirtopphone;?>"><?php echo $agrodirtopphone;?></a>
          </div>

          <a href="<?php echo esc_url(site_url('/')); ?>" class="logo"> <img src="<?php echo esc_attr($top_logo_image); ?>" alt="<?php esc_html_e( 'image', 'agrodir' ); ?>"> </a>
          <a href="#" class="mobile-sidebar-button mobile-sidebar-toggle"><span></span></a>

          <a href="#" class="cart"><i class="fa fa-shopping-cart"></i> <?php esc_html_e( 'Shopping Cart', 'agrodir' ); ?>   (0)</a>
        </div>
      </div> <!-- end .uou-block-2d -->
    </div>
