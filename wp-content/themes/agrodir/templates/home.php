<?php
/**
 * Template Name: Home Page
 *
 */
wp_enqueue_style( 'iv_directories-font', 'https://fonts.googleapis.com/css?family=Raleway');
 ?>
 <?php get_header(); ?>
 <?php
$directory_url_1=get_option('_iv_directory_url_1');
if($directory_url_1==""){$directory_url_1='producers';}

$directory_url_2=get_option('_iv_directory_url_2');
if($directory_url_2==""){$directory_url_2='retailers';}

?>
 <div class="blog-content pbzero home-blog">	
		<?php echo apply_filters('the_content',$post->post_content); ?>
 </div> <!--  end blog-single -->
<?php get_footer();
