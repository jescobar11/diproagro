<?php $agrodir_option_data =get_option('agrodir_option_data');  ?>

<!-- Start Social -->
<?php if(isset($agrodir_option_data['agrodir-share-button']) && $agrodir_option_data['agrodir-share-button'] == 1) : ?>
	<ul class="social">
	<?php if(isset($agrodir_option_data['agrodir-share-button-facebook']) && $agrodir_option_data['agrodir-share-button-facebook'] == 1) : ?>
	<li><a class="fa fa-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url( home_url( '/' ) );?> "></a></li>
	<?php endif; ?>
	<?php if(isset($agrodir_option_data['agrodir-share-button-twitter']) && $agrodir_option_data['agrodir-share-button-twitter'] == 1) : ?>
	<li><a class="fa fa-twitter" href="https://twitter.com/home?status=<?php echo esc_url( home_url( '/' ) ); ?>"></a></li>
	<?php endif; ?>	
	
	<?php if(isset($agrodir_option_data['agrodir-share-button-linkedin']) && $agrodir_option_data['agrodir-share-button-linkedin'] == 1) : ?>	
	<li><a class="fa fa-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo esc_url( home_url( '/' ) );?>"></a></li>
	<?php endif; ?>
	
	<?php if(isset($agrodir_option_data['agrodir-share-button-google']) && $agrodir_option_data['agrodir-share-button-google'] == 1) : ?>	
	<li><a class="fa fa-google-plus" href="https://plus.google.com/share?url=<?php echo esc_url( home_url( '/' ) );?>"></a></li>
	<?php endif; ?>
	
	<?php if(isset($agrodir_option_data['agrodir-share-button-pinterest']) && $agrodir_option_data['agrodir-share-button-pinterest'] == 1) : ?>	
	<li><a class="fa fa-pinterest" href="https://pinterest.com/pin/create/button/?url=<?php echo esc_url( home_url( '/' ) );?>"></a></li>
	<?php endif; ?>
	
	</ul> 
<?php endif; ?>
