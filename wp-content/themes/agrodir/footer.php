</div>
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage simple builder
 * @since 1.0
 */
?>
<?php

$agrodir_option_data =get_option('agrodir_option_data');

$agrodir_option_data['agrodir-multi-footer-image']=1;
$agrodir_option_data['agrodir-multi-bottom-image']=1;
?>
<!-- Start Footer Switch -->

<?php if($agrodir_option_data['agrodir-footer-switch']){?>

<!-- Start agrodir Multifooter -->

<!-- Start Footer 7 -->
<?php if(isset($agrodir_option_data['agrodir-multi-footer-image'])&&($agrodir_option_data['agrodir-multi-footer-image']==1)){?>
  <!-- uou block 4e -->
  <div class="uou-block-4e">
    <div class="container">
      <div class="row">
		  <!-- Contact us section -->
		  <div class="col-md-3 col-sm-6">
			  <?php
							  /** This filter is documented in wp-includes/default-widgets.php */


						$bg_image_default = agrodir_IMAGE.'footer-map-bg.png';
						$title = 	(isset($agrodir_option_data['agrodir-title-contact']) ? $agrodir_option_data['agrodir-title-contact'] :'');
						$logo = 	(isset($agrodir_option_data['agrodir-footer-icon']) ? $agrodir_option_data['agrodir-footer-icon']['url'] : '' );
						$address = (isset($agrodir_option_data['agrodir-address-contact']) ? $agrodir_option_data['agrodir-address-contact'] :'');
						$phone_no = (isset($agrodir_option_data['agrodir-phone-contact']) ? $agrodir_option_data['agrodir-phone-contact'] :'' );
						$email = 	(isset($agrodir_option_data['agrodir-email-contact']) ? $agrodir_option_data['agrodir-email-contact']:'' );
						$bg_image = (isset($agrodir_option_data['agrodir-contact-bg-image']['url']) ? $agrodir_option_data['agrodir-contact-bg-image']['url']: $bg_image_default);
						if($bg_image==''){$bg_image=$bg_image_default;}

						?>

						<!-- <div class="col-md-3 col-sm-6"> -->



						<?php if ( ! empty( $logo ) ) { ?>

							<a href="#" class="logo"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_html_e( 'image', 'agrodir' ); ?>"></a>

						<?php } ?>



							<?php



							if ( ! empty( $bg_image ) ) {

								echo '<ul class="contact-info has-bg-image contain" data-bg-image="'.$bg_image.'">';

							}

							else{

								echo '<ul class="contact-info">';

							}



							if ( ! empty( $address ) ) {

								echo '<li><i class="fa fa-map-marker"></i><address>'.$address.'</address></li>';

							}



							if ( ! empty( $phone_no ) ) {

								echo '<li><i class="fa fa-phone"></i><a href="tel:#">'.$phone_no.'</a></li>';

							}



							if ( ! empty( $email ) ) {

								echo '<li><i class="fa fa-envelope"></i><a href="mailto:">'.$email.'</a></li>';

							}


							?>

						</ul>
		  </div>

         <!-- Start left footer sidebar -->

    <?php   if(isset($agrodir_option_data['agrodir-left-footer-switch'])){ ?>

            <?php

            if(is_active_sidebar('agrodir_footer_left_sidebar')):

				dynamic_sidebar('agrodir_footer_left_sidebar');

            endif;

            ?>

    <?php } ?>

      </div>
    </div>
  </div> <!-- end .uou-block-4e -->
  <?php } ?>


<?php } ?>

<!-- Start Bottom 7 -->
<?php if(isset($agrodir_option_data['agrodir-multi-bottom-image'])&&($agrodir_option_data['agrodir-multi-bottom-image']==1)){?>
  <!-- uou block 4b -->
  <div class="uou-block-4a secondary">
    <div class="container">

      <?php if(isset($agrodir_option_data['agrodir-show-footer-copyrights'])){?>
      <p>
        <?php if(isset($agrodir_option_data['agrodir-copyright-text'])&&!empty($agrodir_option_data['agrodir-copyright-text'])) {?>
        <?php
				if(isset($agrodir_option_data['agrodir-copyright-link']) && $agrodir_option_data['agrodir-copyright-link']!='' ){?>

					<a  href="http://<?php echo esc_html($agrodir_option_data['agrodir-copyright-link']);?> "><?php echo esc_html($agrodir_option_data['agrodir-copyright-text']);?></a>
					<?php
				}else{
					echo esc_html($agrodir_option_data['agrodir-copyright-text']);
				}



				?>
        <?php } ?>
        <?php bloginfo('name'); ?>.
        <?php if(isset($agrodir_option_data['agrodir-after-copyright-text'])&&!empty($agrodir_option_data['agrodir-after-copyright-text'])) {?>
        <?php echo esc_html($agrodir_option_data['agrodir-after-copyright-text']); ?>
        <?php } ?>
        <?php if(isset($agrodir_option_data['agrodir-show-footer-credits']) && $agrodir_option_data['agrodir-show-footer-credits']==1) {?>
        <?php //echo '<a href="http://themeforest.net/user/uouapps">UOUAPPS</a>'; ?>
        <?php } ?>

      </p>
      <?php } ?>


    <!-- Start sccial Profile -->

    <?php if(isset($agrodir_option_data['agrodir-social-profile'])){?>

      <ul class="social-icons">

        <?php if(isset($agrodir_option_data['agrodir-facebook-profile']) && !empty($agrodir_option_data['agrodir-facebook-profile'])) : ?>
        <li><a  href="<?php echo esc_url($agrodir_option_data['agrodir-facebook-profile']);?> "><i class="fa fa-facebook"></i></a></li>
        <?php endif; ?>

        <?php if(isset($agrodir_option_data['agrodir-twitter-profile']) && !empty($agrodir_option_data['agrodir-twitter-profile'])) : ?>
        <li><a  href="<?php echo esc_url($agrodir_option_data['agrodir-twitter-profile']);?> "><i class="fa fa-twitter"></i></a></li>
        <?php endif; ?>

        <?php if(isset($agrodir_option_data['agrodir-google-profile']) && !empty($agrodir_option_data['agrodir-google-profile'])) : ?>
        <li><a  href="<?php echo esc_url($agrodir_option_data['agrodir-google-profile']);?> "><i class="fa fa-google"></i></a></li>
        <?php endif; ?>

        <?php if(isset($agrodir_option_data['agrodir-linkedin-profile']) && !empty($agrodir_option_data['agrodir-linkedin-profile'])) : ?>
        <li><a  href="<?php echo esc_url($agrodir_option_data['agrodir-linkedin-profile']);?> "><i class="fa fa-linkedin"></i></a></li>
        <?php endif; ?>

        <?php if(isset($agrodir_option_data['agrodir-pinterest-profile']) && !empty($agrodir_option_data['agrodir-pinterest-profile'])) : ?>
        <li><a  href="<?php echo esc_url($agrodir_option_data['agrodir-pinterest-profile']);?> "><i class="fa fa-pinterest"></i></a></li>
        <?php endif; ?>

      </ul>

    <?php }?>
    <!-- end of social profile -->

    </div>
  </div>
  <!-- end .uou-block-4a -->
    <?php } ?>



<?php wp_footer(); ?>

</body>
</html>
