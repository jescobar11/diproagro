<?php
/*-------------------------------------------------------------------------
  START JS CSS IMG & VIDEO CONSTANT PATH DEFINED
------------------------------------------------------------------------- */

if(!defined('agrodir_JS')){

	define('agrodir_JS', get_template_directory_uri().'/assets/js/' );
}
if(!defined('agrodir_JS_PLUGINS')){

	define('agrodir_JS_PLUGINS', get_template_directory_uri().'/assets/js/plugins/' );
}
if(!defined('agrodir_CSS')){

	define('agrodir_CSS', get_template_directory_uri().'/assets/css/' );
}

if(!defined('agrodir_IMAGE')){

	define('agrodir_IMAGE', get_template_directory_uri().'/assets/img/');
}

if(!defined('agrodir_VIDEO')){

	define('agrodir_VIDEO', get_template_directory_uri().'/assets/media/');

}

/*-------------------------------------------------------------------------
  END JS CSS AND IMG CONSTANT PATH DEFINED
------------------------------------------------------------------------- */

?>
