<?php
// VC elements
add_action('vc_before_init', 'agrodir_top_banner');
function agrodir_top_banner(){
					vc_map( array(
						  "name" => esc_html__( "Home Top Banner", "agrodir" ),
						  "base" => "agrodir_top",
						  'icon' =>  agrodir_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Agrodir Directory", "agrodir"),
						  "params" => array(						  
						  array(
								"type" => "attach_image",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( " Top Banner Image", "agrodir" ),
								"param_name" => "top_banner",								
								),							 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "agrodir" ),
								"param_name" => "top_title",
								"value" => esc_html__( "Retailers Directory", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "agrodir" ),
								"param_name" => "top_sub_title",
								"value" => esc_html__( "SEARCH FOR Producers AND Retailers ON WORLD WIDE BASIS", "agrodir" ),								
							 ),
							
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "producers Button Text", "agrodir" ),
								"param_name" => "producers_button_text",
								"value" => esc_html__( "FIND A producers", "agrodir" ),								
							 ),
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "producers Button Link", "agrodir" ),
								"param_name" => "producers_button_link",
								"value" => esc_html__( "", "agrodir" ),								
							 ),	
							 					 
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "retailer Button Text", "agrodir" ),
								"param_name" => "retailer_button_text",
								"value" => esc_html__( "FIND A retailer", "agrodir" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "retailer Button Link", "agrodir" ),
								"param_name" => "retailer_button_link",
								"value" => esc_html__( "", "agrodir" ),								
							 ),	
							  array(
								"type" => "checkbox",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Banner Search Bar", "agrodir" ),
								"param_name" => "top_search_bar",
								"value" => esc_html__( "1", "agrodir" ),								
							 ),
						  )
					   ) );
				
				}
add_shortcode('agrodir_top', 'agrodir_top_func');	

function agrodir_top_func($atts, $content = null ){	
									
	include('vc/top_banner.php');				
}	
//********* 3 home page blocks
add_action('vc_before_init', 'agrodir_top_blocks');
function agrodir_top_blocks(){
					vc_map( array(
						  "name" => esc_html__( "Home Top 3 Blocks", "agrodir" ),
						  "base" => "agrodir_top_3blocks",
						  'icon' =>  agrodir_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Agrodir Directory", "agrodir"),
						  "params" => array(
						  array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Color", "agrodir" ),
								"param_name" => "block1_color",
								"value" => esc_html__( "#f5f5f5", "agrodir" ),
								
								
							 ),
						   array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Top Icon", "agrodir" ),
								"param_name" => "block1_top_icon",
								"value" => esc_html__( "fa-briefcase", "agrodir" ),
								"description" => __( "You can more Icon code here : https://fontawesome.com/v4.7.0/icons/ ", "agrodir" )
								
							 ),
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Top Title", "agrodir" ),
								"param_name" => "b1top_title",
								"value" => esc_html__( "producers", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Sub Title", "agrodir" ),
								"param_name" => "b1top_sub_title",
								"value" => esc_html__( "With Over 300 producers across 20 countries falcon directory is the right place to find your closest agro office", "agrodir" ),								
							 ),
							 
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Button Text", "agrodir" ),
								"param_name" => "b1button_title",
								"value" => esc_html__( "SEARCH NOW", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Button Link", "agrodir" ),
								"param_name" => "b1top_button_link",
								"value" => esc_html__( "", "agrodir" ),								
							 ),							 
							
							 array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Color", "agrodir" ),
								"param_name" => "block2_color",
								"value" => esc_html__( "#f5f5f5", "agrodir" ),
								
								
							 ),
							   array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Top Icon", "agrodir" ),
								"param_name" => "block2_top_icon",
								"value" => esc_html__( "fa-black-tie", "agrodir" ),
								"description" => __( "You can more Icon code here : https://fontawesome.com/v4.7.0/icons/ ", "agrodir" )
								
							 ),
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Top Title", "agrodir" ),
								"param_name" => "b2top_title",
								"value" => esc_html__( "retailer", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Sub Title", "agrodir" ),
								"param_name" => "b2top_sub_title",
								"value" => esc_html__( "Find the right retailer within the closest producers across a wide range of agro fields including dairy", "agrodir" ),								
							 ),
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Button Text", "agrodir" ),
								"param_name" => "b2button_title",
								"value" => esc_html__( "SEARCH NOW", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Button Link", "agrodir" ),
								"param_name" => "b2top_button_link",
								"value" => esc_html__( "", "agrodir" ),								
							 ),	
							  array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Color", "agrodir" ),
								"param_name" => "block3_color",
								"value" => esc_html__( "#f5f5f5", "agrodir" ),
								
								
							 ),	
							   array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Top Icon", "agrodir" ),
								"param_name" => "block3_top_icon",
								"value" => esc_html__( "fa-diamond", "agrodir" ),
								"description" => __( "You can more Icon code here : https://fontawesome.com/v4.7.0/icons/ ", "agrodir" )
								
							 ),
							array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Top Title", "agrodir" ),
								"param_name" => "b3top_title",
								"value" => esc_html__( "Register Now", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Sub Title", "agrodir" ),
								"param_name" => "b3top_sub_title",
								"value" => esc_html__( "You're a agro center with producerss and Retailers worldwide, Retailers Directory is the right place to list your producerss and Retailers, join us now", "agrodir" ),								
							 ), 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Button Text", "agrodir" ),
								"param_name" => "b3button_title",
								"value" => esc_html__( "Register", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Button Link", "agrodir" ),
								"param_name" => "b3top_button_link",
								"value" => esc_html__( "", "agrodir" ),								
							 ),		
						  
							 
							 
						  )
					   ) );
				
				}
add_shortcode('agrodir_top_3blocks', 'agrodir_top_3blocks_func');	

function agrodir_top_3blocks_func($atts, $content = null ){	
									
	include('vc/top_3blocks.php');				
}	
// CPT 1 category
$directory_url_1=get_option('_iv_directory_url_1');					
if($directory_url_1==""){$directory_url_1='producers';}	

add_action('vc_before_init', 'agrodir_cpt1_category');
function agrodir_cpt1_category(){
					vc_map( array(
						  "name" => esc_html__( "Producers Categories", "agrodir" ),
						  "base" => "agrodir_cpt1_category",
						  'icon' =>  agrodir_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Agrodir Directory", "agrodir"),
						  "params" => array(
						  						 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "agrodir" ),
								"param_name" => "cpt1_category_title",
								"value" => esc_html__( "producers Categories", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "agrodir" ),
								"param_name" => "cpt1_category_sub_title",
								"value" => esc_html__( "With over 3000 producers offeres across 20 countries agrodir is the right place to find your closest agro service provider thal will help you in court", "agrodir" ),								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('agrodir_cpt1_category', 'agrodir_cpt1_category_func');	

function agrodir_cpt1_category_func($atts, $content = null ){	
									
	include('vc/ctp1_category.php');				
}

// CPT 2 category


add_action('vc_before_init', 'agrodir_cpt2_category');
function agrodir_cpt2_category(){
					vc_map( array(
						  "name" => esc_html__( "Retailer Categories", "agrodir" ),
						  "base" => "agrodir_cpt2_category",
						  'icon' =>  agrodir_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Agrodir Directory", "agrodir"),
						  "params" => array(
						    array(
								"type" => "attach_image",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( " Background Image", "agrodir" ),
								"param_name" => "cpt2_category_image",								
								),		
						  						 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "agrodir" ),
								"param_name" => "cpt2_category_title",
								"value" => esc_html__( "Retailer Categories", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "agrodir" ),
								"param_name" => "cpt2_category_sub_title",
								"value" => esc_html__( "With over 3000 producers offeres across 20 countries agrodir is the right place to find your closest agro service provider thal will help you in court", "agrodir" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Categores Only", "agrodir" ),
								"param_name" => "cpt2_category_only_slug",
								"description" => __( "You can add category slugs", "agrodir" )								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('agrodir_cpt2_category', 'agrodir_cpt2_category_func');	

function agrodir_cpt2_category_func($atts, $content = null ){	
									
	include('vc/ctp2_category.php');				
}
/// Feature producers

add_action('vc_before_init', 'agrodir_ctp1_featured');
function agrodir_ctp1_featured(){
					vc_map( array(
						  "name" => esc_html__( "producers Featured", "agrodir" ),
						  "base" => "agrodir_ctp1_featured",
						  'icon' =>  agrodir_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Agrodir Directory", "agrodir"),
						  "params" => array(
						    array(
								"type" => "attach_image",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( " Background Image", "agrodir" ),
								"param_name" => "cpt1_featured_image",								
								),		
						  						 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "agrodir" ),
								"param_name" => "cpt1_featured_title",
								"value" => esc_html__( "Featured producers", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "agrodir" ),
								"param_name" => "cpt1_featured_sub_title",
								"value" => esc_html__( "With Over 300 agro firm across 20 countries falcon directory is the right place to find your closest agro office", "agrodir" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Featured producers IDs", "agrodir" ),
								"param_name" => "cpt1_featured_ids",
								"description" => __( "10,20,36", "agrodir" )								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('agrodir_ctp1_featured', 'agrodir_ctp1_featured_func');	

function agrodir_ctp1_featured_func($atts, $content = null ){	
									
	include('vc/ctp1_featured.php');				
}


// CPT 2 featured


add_action('vc_before_init', 'agrodir_ctp2_featured');
function agrodir_ctp2_featured(){
					vc_map( array(
						  "name" => esc_html__( "Retailer Featured", "agrodir" ),
						  "base" => "agrodir_ctp2_featured",
						  'icon' =>  agrodir_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Agrodir Directory", "agrodir"),
						  "params" => array(
						    array(
								"type" => "attach_image",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( " Background Image", "agrodir" ),
								"param_name" => "cpt2_featured_image",								
								),		
						  						 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "agrodir" ),
								"param_name" => "cpt2_featured_title",
								"value" => esc_html__( "Featured retailer", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "agrodir" ),
								"param_name" => "cpt2_featured_sub_title",
								"value" => esc_html__( "With over 5000 Retailers and experts in the healthcare field Retailers Directory provides a listing of all Retailers
across a wide variety if agro fields", "agrodir" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Featured retailer IDs", "agrodir" ),
								"param_name" => "cpt2_featured_ids",
								"description" => __( "10,20,36", "agrodir" )								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('agrodir_ctp2_featured', 'agrodir_ctp2_featured_func');	

function agrodir_ctp2_featured_func($atts, $content = null ){	 
									
	include('vc/ctp2_featured.php');				
}

// Latest Post
add_action('vc_before_init', 'agrodir_latest_post');
function agrodir_latest_post(){
					vc_map( array(
						  "name" => esc_html__( "Latest Post", "agrodir" ),
						  "base" => "agrodir_latest_post",
						  'icon' =>  agrodir_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Agrodir Directory", "agrodir"),
						  "params" => array(
						   			 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "agrodir" ),
								"param_name" => "latest_post_title",
								"value" => esc_html__( "Latest Post", "agrodir" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "agrodir" ),
								"param_name" => "latest_post_sub_title",
								"value" => esc_html__( "With over 5000 Retailers and experts in the agro field Retailers Directory provides a listing of all Retailers
across a wide variety if agro fields", "agrodir" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Post IDs", "agrodir" ),
								"param_name" => "latest_post_ids",
								"description" => __( "10,20,36", "agrodir" )								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('agrodir_latest_post', 'agrodir_latest_post_func');	

function agrodir_latest_post_func($atts, $content = null ){	 
									
	include('vc/latest_post.php');				
}

