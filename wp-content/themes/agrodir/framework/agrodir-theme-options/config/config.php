<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "agrodir_option_data";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();
    
    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => false,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Agrodir Theme Options', 'redux-framework-demo' ),
        'page_title'           => __( 'Agrodir Theme Options', 'redux-framework-demo' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => false,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => false,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        //'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => false,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => __( 'Extensions', 'redux-framework-demo' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */
     $directory_url_1=get_option('_iv_directory_url_1');
	if($directory_url_1==""){$directory_url_1='producers';}

	$directory_url_2=get_option('_iv_directory_url_2');
	if($directory_url_2==""){$directory_url_2='retailers';}
	
	$directory_url_1_string=str_replace("-"," ",$directory_url_1); $directory_url_1_string = esc_attr (ucwords($directory_url_1_string));	
	$directory_url_2_string=str_replace("-"," ",$directory_url_2); $directory_url_2_string = esc_attr (ucwords($directory_url_2_string));
	

    // -> START Basic Fields
   
    
   //--------------------

     /*
    |--------------------------------------------------------------------------
    | start header settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */

           Redux::setSection( $opt_name, array(
                'icon'      => 'el-icon-paper-clip',
                'title'     => esc_html__('Header Settings', 'agrodir'),
                'fields'    => array(


                    array(
                        'id'        => 'agrodir-header-icon',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Header Logo', 'agrodir'),
                        'compiler'  => 'true',
                        'subtitle'      => esc_html__('Upload header logo.', 'agrodir'),
                        
                      

                    ),
                    array(
                        'id'        => 'agrodir-breadcrumb',
                        'type'      => 'switch',
                        'title'     => esc_html__('Top breadcrumb Show/hide', 'tiger'),
                        'default'   => true,
                    ),
                    
                     array(
                        'id'        => 'agrodir-banner-breadcrumb',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Banner Breadcrumb', 'tiger'),
                        'compiler'  => 'true',
                        'subtitle'      => esc_html__('Upload banner breadcrumb image', 'tiger'),
                        
                      

                    ),
					
					 array(
                        'id'        => 'agrodir-top-phone',
                        'type'      => 'text',                        
                        'title'     => esc_html__('Top Phone #', 'tiger'),
                        'compiler'  => 'true',
                        'subtitle'      => esc_html__('Add top phone number', 'tiger'),
                        
                      

                    ),

                     array(
                        'id'        => 'agrodir-footer-icon',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Footer Logo', 'agrodir'),
                        'compiler'  => 'true',
                        'subtitle'      => esc_html__('Upload footer logo.', 'agrodir'),
                        
                      

                    ),



                    array(
                        'id'        => 'agrodir-share-button',
                        'type'      => 'switch',
                        'title'     => esc_html__('Share button', 'agrodir'),
                        'default'   => true,
                    ),


                    array(
                        'id'        => 'agrodir-share-button-facebook',
                        'type'      => 'switch',
                        'title'     => esc_html__(' Facebook Share button', 'agrodir'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'agrodir-share-button-twitter',
                        'type'      => 'switch',
                        'title'     => esc_html__(' Twitter Share button', 'agrodir'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'agrodir-share-button-google',
                        'type'      => 'switch',
                        'title'     => esc_html__(' Google Plus Share button', 'agrodir'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'agrodir-share-button-linkedin',
                        'type'      => 'switch',
                        'title'     => esc_html__(' LinkedIn Share button', 'agrodir'),
                        'default'   => false,
                    ),                  

                    array(
                        'id'        => 'agrodir-share-button-pinterest',
                        'type'      => 'switch',
                        'title'     => esc_html__(' Pinterest Share button', 'agrodir'),
                        'default'   => false,
                    ),


                    array(
                        'id'        => 'agrodir-top-language',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Language ', 'agrodir'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'agrodir-language',
                        'type'      => 'multi_text',
                        'title'     => esc_html__('Language', 'agrodir'),
                        'required'  => array('agrodir-top-language', '=', '1'),
                        'default'   => array(
                            'en' => 'EN',
                            'de' => 'DE',
                            'it' => 'IT',
                            'fr' => 'FR',
                        )
                    ),


                    array(
                        'id'        => 'agrodir-login-option',
                        'type'      => 'switch',
                        'title'     => esc_html__('Login option', 'agrodir'),
                        'default'   => true,
                    ),


                    array(
                        'id'       => 'agrodir-wpml-select',
                        'type'     => 'select',
                        'title'    => esc_html__('WPML language show type', 'agrodir'),
                        'subtitle' => esc_html__('Select the type how you want to show language selector', 'agrodir'),
                        'desc'     => esc_html__('This select type will only work if WPML activated in your theme', 'agrodir'),

                        'options'  => array(
                            'code' => 'Language Code',
                            'name' => 'Language Name',
                            'flag' => 'Flag'
                        ),
                        'default'  => 'name',
                    ),



                )
            ));
             /*
    |--------------------------------------------------------------------------
    | Start agrodir Multi Top Bar Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */

       Redux::setSection( $opt_name, array(
            'icon'      => 'el el-cog',
            'title'     => esc_html__('Multi Header Options', 'agrodir'),
            'fields'    => array(

                array(
                    'id'        => 'agrodir-header-switch',
                    'type'      => 'switch',
                    'title'     => esc_html__('Show Header', 'agrodir'),
                    'subtitle'  => esc_html__('Decide to show Header or Not', 'agrodir'),
                    'default'   => true,
                    ),
                
                array(
                    'id'        => 'agrodir-multi-header-image',
                    'type'      => 'image_select',
                    'title'     => esc_html__('agrodir Header images', 'agrodir'),
                    'subtitle'  => esc_html__('Select Which Header Image to show', 'agrodir'),
                    'options'  => Array(
                        '1'      =>  Array (
                                 'alt'  => 'default',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header1.png',
                            ),

                        '2'      =>  Array (
                                 'alt'  => 'Header 1',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header2.png',
                            ),

                        '3'      =>  Array (
                                 'alt'  => 'Header 2',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header3.png',
                            ),

                        '4'      =>  Array (
                                 'alt'  => 'Header 3',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header4.png',
                            ),

                        // '5'      =>  Array (
                        //          'alt'  => 'Header 4',
                        //          'img'  =>  ReduxFramework::$_url.'assets/img/header/header5.png',
                        //     ),

                        // '6'      =>  Array (
                        //          'alt'  => 'Header 5',
                        //          'img'  =>  ReduxFramework::$_url.'assets/img/header/header6.png',
                        //     ),

                        '7'      =>  Array (
                                 'alt'  => 'Header 6',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header7.png',
                            ),

                        '8'      =>  Array (
                                 'alt'  => 'Header 7',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header8.png',
                            ),

                        '9'      =>  Array (
                                 'alt'  => 'Header 8',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header9.png',
                            ),

                        '10'      =>  Array (
                                 'alt'  => 'Header 9',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header10.png',
                            ),

                        '11'      =>  Array (
                                 'alt'  => 'Header 10',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header11.png',
                            ),

                        '12'      =>  Array (
                                 'alt'  => 'Header 11',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header12.png',
                            ),

                        '13'      =>  Array (
                                 'alt'  => 'Header 12',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header13.png',
                            ),

                        '14'      =>  Array (
                                 'alt'  => 'Header 13',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header14.png',
                            ),

                        '15'      =>  Array (
                                 'alt'  => 'Header 14',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header15.png',
                            ),

                        '16'      =>  Array (
                                 'alt'  => 'Header 15',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/header/header16.png',
                            ),

                        ),
                    'default'   => 1,
                ),
            )
        ));
        Redux::setSection( $opt_name, array(
            'icon'      => 'el el-cog',
            'title'     => esc_html__('agrodir Multi TopBar Options', 'agrodir'),
            'fields'    => array(

                array(
                    'id'        => 'agrodir-top-bar-switch',
                    'type'      => 'switch',
                    'title'     => esc_html__('Show Tob Bar', 'agrodir'),
                    'subtitle'  => esc_html__('Decide to show Tob Bar or Not', 'agrodir'),
                    'default'   => true,
                    ),
                
                array(
                    'id'        => 'agrodir-multi-topBar-image',
                    'type'      => 'image_select',
                    'title'     => esc_html__('agrodir Top Bar images', 'agrodir'),
                    'subtitle'  => esc_html__('Select Which topBar Image to show', 'agrodir'),
                    'options'  => Array(
                        '1'      =>  Array (
                                 'alt'  => 'TopBar 1',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top1.png',
                            ),

                        '2'      =>  Array (
                                 'alt'  => 'TopBar 2',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top2.png',
                            ),

                        '3'      =>  Array (
                                 'alt'  => 'TopBar 3',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top3.png',
                            ),

                        '4'      =>  Array (
                                 'alt'  => 'TopBar 4',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top4.png',
                            ),

                        '5'      =>  Array (
                                 'alt'  => 'TopBar 5',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top5.png',
                            ),

                        '6'      =>  Array (
                                 'alt'  => 'Default Top Bar',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top6.png',
                            ),
                        '7'      =>  Array (
                                 'alt'  => 'Default Top Bar',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top7.png',
                            ),
                        '8'      =>  Array (
                                 'alt'  => 'Default Top Bar',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top8.png',
                            ),
                        '9'      =>  Array (
                                 'alt'  => 'Default Top Bar',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top9.png',
                            ),
                        '10'      =>  Array (
                                 'alt'  => 'Default Top Bar',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top10.png',
                            ),
                        '11'      =>  Array (
                                 'alt'  => 'Default Top Bar',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top11.png',
                            ),
                        '12'      =>  Array (
                                 'alt'  => 'Default Top Bar',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top12.png',
                            ),
                        '13'      =>  Array (
                                 'alt'  => 'Default Top Bar',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/topBar/top13.png',
                            ),

                        ),
                    'default'   => 1,
                ),
            )
        ));


    /*
    |--------------------------------------------------------------------------
    | End  agrodir Multi Top Bar Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */




     /*
    |--------------------------------------------------------------------------
    | Start agrodir Multi Breadcrumb Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */

Redux::setSection( $opt_name, array(
            'icon'      => 'el el-cog',
            'title'     => esc_html__('Multi-Breadcrumb Options', 'agrodir'),
            'fields'    => array(

                array(
                    'id'        => 'agrodir-breadcrumb-switch',
                    'type'      => 'switch',
                    'title'     => esc_html__('Show Breadcrumb', 'agrodir'),
                    'subtitle'  => esc_html__('Decide to show Breadcrumb or Not', 'agrodir'),
                    'default'   => true,
                    ),
                
                array(
                    'id'        => 'agrodir-multi-breadcrumb-image',
                    'type'      => 'image_select',
                    'title'     => esc_html__('agrodir Breadcrumb images', 'agrodir'),
                    'subtitle'  => esc_html__('Select Which breadcrumb Image to show', 'agrodir'),
                    'options'  => Array(
                        '1'      =>  Array (
                                 'alt'  => 'default',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb1.png',
                            ),

                        '2'      =>  Array (
                                 'alt'  => 'Breadcrumb 1',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb2.png',
                            ),

                        '3'      =>  Array (
                                 'alt'  => 'Breadcrumb 2',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb3.png',
                            ),

                        '4'      =>  Array (
                                 'alt'  => 'Breadcrumb 3',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb4.png',
                            ),

                        '5'      =>  Array (
                                 'alt'  => 'Breadcrumb 4',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb5.png',
                            ),

                        '6'      =>  Array (
                                 'alt'  => 'Breadcrumb 5',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb6.png',
                            ),
						/*
                        '7'      =>  Array (
                                 'alt'  => 'Breadcrumb 6',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb7.png',
                            ),

                        '8'      =>  Array (
                                 'alt'  => 'Breadcrumb 7',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb8.png',
                            ),

                        '9'      =>  Array (
                                 'alt'  => 'Breadcrumb 8',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb9.png',
                            ),
						*/	
                        '10'      =>  Array (
                                 'alt'  => 'Breadcrumb 9',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb10.png',
                            ),

                        '11'      =>  Array (
                                 'alt'  => 'Breadcrumb 10',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb11.png',
                            ),

                        '12'      =>  Array (
                                 'alt'  => 'Breadcrumb 11',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb12.png',
                            ),

                        '13'      =>  Array (
                                 'alt'  => 'Breadcrumb 12',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb13.png',
                            ),

                        '14'      =>  Array (
                                 'alt'  => 'Breadcrumb 13',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb14.png',
                            ),

                        '15'      =>  Array (
                                 'alt'  => 'Header 14',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb15.png',
                            ),

                        '16'      =>  Array (
                                 'alt'  => 'Header 15',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/breadcrumbs/crumb16.png',
                            ),

                        ),
                    'default'   => 1,
                ),
            )
        ));


     /*
    |--------------------------------------------------------------------------
    | End agrodir Multi Breadcrumb Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */
     /*
    |--------------------------------------------------------------------------
    | Start agrodir Multi Top Bar Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */

       Redux::setSection( $opt_name, array(
            'icon'      => 'el el-cog',
            'title'     => esc_html__('agrodir Multi Blog Options', 'agrodir'),
            'fields'    => array(

                array(
                    'id'        => 'agrodir-blog-switch',
                    'type'      => 'switch',
                    'title'     => esc_html__('Show Tob Bar', 'agrodir'),
                    'subtitle'  => esc_html__('Decide to show Tob Bar or Not', 'agrodir'),
                    'default'   => true,
                    ),
                
                array(
                    'id'        => 'agrodir-multi-blog-image',
                    'type'      => 'image_select',
                    'title'     => esc_html__('agrodir Blog images', 'agrodir'),
                    'subtitle'  => esc_html__('Select Which Blog Image to show', 'agrodir'),
                    'options'  => Array(
                        '1'      =>  Array (
                                 'alt'  => 'blog 1',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/SB-blog/blog1.png',
                            ),

                        '2'      =>  Array (
                                 'alt'  => 'blog 1',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/SB-blog/blog2.png',
                            ),

                        '3'      =>  Array (
                                 'alt'  => 'blog 1',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/SB-blog/blog3.png',
                            ),

                        '4'      =>  Array (
                                 'alt'  => 'blog 1',
                                 'img'  =>  ReduxFramework::$_url.'assets/img/SB-blog/blog4.png',
                            ),
                        ),
                    'default'   => 1,
                ),
            )
        ));


    /*
    |--------------------------------------------------------------------------
    | End  agrodir Multi Top Bar Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */
   



 
    /*
    |--------------------------------------------------------------------------
    | Start Construction settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */


        Redux::setSection( $opt_name, array(

            'icon'      => 'el el-cog',
            'title'     => esc_html__('agrodir Template', 'agrodir'),
            'fields'    => array(


                array(
                        'id'          => 'agrodir-construction-header-slider',
                        'type'        => 'slides',
                        'title'       => esc_html__('agrodir Slider Imeges', 'agrodir'),
                        'subtitle'    => esc_html__('agrodir Unlimited slides with drag and drop sortings.', 'agrodir'),
                        ),


                    array(
                        'id'        => 'agrodir-construction-testimonial-footer-banner-image',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('agrodir testimonial Image', 'agrodir'),
                        'compiler'  => 'true',
                        'desc'      => esc_html__('change your testimonial footer banner image', 'agrodir'),
                    ),
        ),
));



 

            


     /*
    |--------------------------------------------------------------------------
    | Start Social Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */

            Redux::setSection( $opt_name, array(

                'icon'  => 'el-icon-myspace',
                'title' => esc_html__('Social Profile Link', 'agrodir'),

                'fields' => array(

                    array(
                        'id'        => 'agrodir-social-profile',
                        'type'      => 'switch',
                        'title'     => esc_html__('Social Profile', 'agrodir'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'agrodir-social-profile-title',
                        'type'      => 'text',
                        'title'     => esc_html__('Social Profile Title', 'agrodir'),

                    ),

                    array(
                        'id'        => 'agrodir-facebook-profile',
                        'type'      => 'text',
                        'title'     => esc_html__('Facebook', 'agrodir'),

                    ),

                    array(
                        'id'        => 'agrodir-twitter-profile',
                        'type'      => 'text',
                        'title'     => esc_html__('Twitter', 'agrodir'),
                    ),

                    array(
                        'id'        => 'agrodir-google-profile',
                        'type'      => 'text',
                        'title'     => esc_html__('Google', 'agrodir'),

                    ),

                    array(
                        'id'        => 'agrodir-linkedin-profile',
                        'type'      => 'text',
                        'title'     => esc_html__('Linkedin', 'agrodir'),

                    ),

                    array(
                        'id'        => 'agrodir-pinterest-profile',
                        'type'      => 'text',
                        'title'     => esc_html__('Pinterest', 'agrodir'),

                    ),

                )
            ));

    /*
    |--------------------------------------------------------------------------
    | End Social Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */

  








    /*
    |--------------------------------------------------------------------------
    | Start Footer Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */


       Redux::setSection( $opt_name, array(
            'icon'      => 'el-icon-photo',
            'title'     => esc_html__('Footer Options', 'agrodir'),
            'fields'    => array(

                array(
                    'id'        => 'agrodir-left-footer-switch',
                    'type'      => 'switch',
                    'title'     => esc_html__('Show Footer Widgets', 'agrodir'),
                    'subtitle'  => esc_html__('Decide show agrodir Footer Widgets or Not', 'agrodir'),
                    'default'   => true,
                ),
				 array(
                    'id'        => 'agrodir-footer-menu-switch',
                    'type'      => 'switch',
                    'title'     => esc_html__('Show Footer Menu', 'agrodir'),
                    'subtitle'  => esc_html__('Decide to show Footer Menu or Not', 'agrodir'),
                    'default'   => true,
                ),

                array(
                    'id'        => 'agrodir-footer-switch',
                    'type'      => 'switch',
                    'title'     => esc_html__('Show Footer', 'agrodir'),
                    'subtitle'  => esc_html__('Decide to show Footer or Not', 'agrodir'),
                    'default'   => true,
                ),				
                
                array(
                    'id'        => 'agrodir-show-footer-copyrights',
                    'type'      => 'switch',
                    'title'     => esc_html__('Show Footer Copyrights', 'agrodir'),
                    'subtitle'  => esc_html__('Decide show Footer copyrights or Not', 'agrodir'),
                    'default'   => true,
                ),
				 array(
                    'id'        => 'agrodir-show-footer-credits',
                    'type'      => 'switch',
                    'title'     => esc_html__('Show Footer Credits of uouapps', 'agrodir'),
                    'subtitle'  => esc_html__('Decide show Footer Credits of uouapp or Not', 'agrodir'),
                    'default'   => true,
                ), 
				
				
                 array(
                    'id'        => 'agrodir-address-contact',
                    'type'      => 'text',
                    'title'     => esc_html__('Contact address', 'agrodir'),
                    'subtitle'  => esc_html__('Enter address', 'agrodir'),
                    'placeholder' => esc_html__('Enter address','agrodir'),
                    'default'  => esc_html__("350 Fifth Avenue, 34th floor, New York, NY 10118-3299 USA",'agrodir'), 
                ),
                
                
                
                 array(
                    'id'        => 'agrodir-contact-bg-image',
                    'type'      => 'media',
                    'title'     => esc_html__('Contact Background Image', 'agrodir'),
                    'subtitle'  => esc_html__('Select Background Image', 'agrodir'),
                    'placeholder' => esc_html__('Select Background Image','agrodir'),
                ),
                 array(
                    'id'        => 'agrodir-email-contact',
                    'type'      => 'text',
                    'title'     => esc_html__('Contact email', 'agrodir'),
                    'subtitle'  => esc_html__('Enter email', 'agrodir'),
                    'placeholder' => esc_html__('Enter email','agrodir'),
                    'default'  => 'test@test.com',
                ),
                 array(
                    'id'        => 'agrodir-phone-contact',
                    'type'      => 'text',
                    'title'     => esc_html__('Contact phone', 'agrodir'),
                    'subtitle'  => esc_html__('Enter phone', 'agrodir'),
                    'placeholder' => esc_html__('Enter phone','agrodir'),
                    'default'  => esc_html__('+4534435345','agrodir'), '',
                ),
                
                
                array(
                    'id'        => 'agrodir-copyright-text',
                    'type'      => 'text',
                    'title'     => esc_html__('Copyright Text', 'agrodir'),
                    'subtitle'  => esc_html__('Enter your copyright text', 'agrodir'),
                    'placeholder' => esc_html__('&copy; Copyright 2016 ','agrodir'),
                    'default'  => esc_html__('&copy; Copyright 2016 ', 'agrodir'), 
                    
                ),
				 array(
                    'id'        => 'agrodir-copyright-link',
                    'type'      => 'text',
                    'title'     => esc_html__('Copyright link', 'agrodir'),
                    'subtitle'  => esc_html__('Enter your copyright link', 'agrodir'),
                    'placeholder' => esc_html__('www.example.com ','agrodir'),
                    
                    
                ),
                array(
                    'id'        => 'agrodir-after-copyright-text',
                    'type'      => 'text',
                    'title'     => esc_html__('After Copyright Text', 'agrodir'),
                    'subtitle'  => esc_html__('Enter your After copyright text', 'agrodir'),
                    'placeholder' => esc_html__('All rights reserved.','agrodir'),
                    'default'  =>esc_html__(' All rights reserved. ', 'agrodir'), 
                ),

                array(
                    'id'        => 'agrodir-privacy',
                    'type'      => 'text',
                    'title'     => esc_html__('Privacy Policy', 'agrodir'),
                    'subtitle'  => esc_html__('Enter your company Privacy Policy link', 'agrodir'),
                    'placeholder' => esc_html__('www.example.com','agrodir'),
                    'default'  => ''
                ),

                array(
                    'id'        => 'agrodir-condition',
                    'type'      => 'text',
                    'title'     => esc_html__('Terms & Conditions', 'agrodir'),
                    'subtitle'  => esc_html__('Enter your Terms & Conditions link', 'agrodir'),
                    'placeholder' => esc_html__('www.example.com','agrodir'),                    
                    'default'  => '',
                ),
                 
            )
        ));




    /*
    |--------------------------------------------------------------------------
    | End Footer Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */


    /*
    |--------------------------------------------------------------------------
    | Start Styling Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */


           Redux::setSection( $opt_name, array(
                'icon'      => 'el-icon-website',
                'title'     => esc_html__('Styling Options', 'agrodir'),
                'fields'    => array(


                    array(
                        'id'        => 'agrodir-select-stylesheet',
                        'type'      => 'select',
                        'title'     => esc_html__('Theme Stylesheet', 'agrodir'),
                        'subtitle'  => esc_html__('Select your themes alternative color scheme.', 'agrodir'),
                        'options'   => array(
                            'style-switcher.css' => 'Default',
                            'gold.css' => 'Gold',
                        ),
                        'default'   => 'style-switcher.css',
                    ),


                    array(
                        'id'            => 'agrodir-body-typography',
                        'type'          => 'typography',
                        'title'         => esc_html__('Body', 'agrodir'),
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => true,    // Select a backup non-google font in addition to a google font
                        'font-style'    => false, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => false, // Only appears if google is true and subsets not set to false
                        'font-size'     => false,
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        => array('body'), // An array of CSS selectors to apply this font style to dynamically
                        'units'         => 'px', // Defaults to px
                        'default'   => '',

                    ),



                    array(
                        'id'            => 'agrodir-header-typography',
                        'type'          => 'typography',
                        'title'         => esc_html__('Header', 'agrodir'),
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => true,    // Select a backup non-google font in addition to a google font
                        'font-style'    => false, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => false, // Only appears if google is true and subsets not set to false
                        'font-size'     => false,
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        => array('h1','h2','h3','h4','h5'), // An array of CSS selectors to apply this font style to dynamically

                        'units'         => 'px', // Defaults to px
                        'default'   => '',
                    ),

                )
            ));


    /*
    |--------------------------------------------------------------------------
    | End Styling Options settings
    |--------------------------------------------------------------------------
    |
    |
    |
    */





 








    if ( file_exists( dirname( __FILE__ ) . '/../README.md' ) ) {
        $section = array(
            'icon'   => 'el el-list-alt',
            'title'  => __( 'Documentation', 'redux-framework-demo' ),
            'fields' => array(
                array(
                    'id'       => '17',
                    'type'     => 'raw',
                    'markdown' => true,
                    'content_path' => dirname( __FILE__ ) . '/../README.md', // FULL PATH, not relative please
                    //'content' => 'Raw content here',
                ),
            ),
        );
        Redux::setSection( $opt_name, $section );
    }
    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $field['msg']    = 'your custom error message';
                $return['error'] = $field;
            }

            if ( $warning == true ) {
                $field['msg']      = 'your custom warning message';
                $return['warning'] = $field;
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            $args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

