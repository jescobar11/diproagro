<?php


$radius=get_option('_iv_radius');
$agrodir_option_data =get_option('agrodir_option_data');
$keyword_post='';
$back_ground_color='0099fe';
if(isset($atts['bgcolor']) and $atts['bgcolor']!="" ){
	$back_ground_color=$atts['bgcolor'];
}
$directory_url_1=get_option('_iv_directory_url_1');					
if($directory_url_1==""){$directory_url_1='producers';}	

$directory_url_2=get_option('_iv_directory_url_2');					
if($directory_url_2==""){$directory_url_2='retailers';}	

$directory_url_1_string=str_replace("-"," ",$directory_url_1); 
$directory_url_1_string =  esc_attr (ucwords($directory_url_1_string)); 						
 						 
$directory_url_1_string = (isset($agrodir_option_data['agrodir-home-hearder-block1'])? $agrodir_option_data['agrodir-home-hearder-block1']: $directory_url_1_string);

$directory_url_2_string=str_replace("-"," ",$directory_url_2); 
$directory_url_2_string= (isset($agrodir_option_data['agrodir-home-hearder-block2'])?$agrodir_option_data['agrodir-home-hearder-block2']:$directory_url_2_string)


?>

<form  action="<?php echo get_post_type_archive_link( $directory_url_1) ; ?>" method="POST"  class="form-inline advanced-serach" id="searchformhd" onkeypress="return event.keyCode != 13;">
	<div class="container">
	 <div class="input-field">

			 <div class="" >
          <div class="form-group" >
					   <input type="text" class="cbp-search-input" id="keyword" name="keyword"  placeholder="<?php esc_html_e( 'Filter By Keyword', 'agrodir' ); ?>" value="<?php echo $keyword_post; ?>">
			     </div>
        </div>


				<div class="" >
					<div class="form-group" >
						<input type="text" class="cbp-search-input location-input" id="address" name="address"  placeholder="<?php esc_html_e( 'Location', 'agrodir' ); ?>"
						value="">
						<i class="fa fa-map-marker marker"></i>
						<input type="hidden" id="latitude" name="latitude"  value="" >
						<input type="hidden" id="longitude" name="longitude"  value="">
					</div>
			  </div>
			  
			 <div class="" >
				  <div class="form-group" >
						<i class="fa fa-chevron-down arrow"></i>
						<select name="dir_specialities"  id="dir_specialities" class="cbp-search-select">
							<option  class="cbp-search-select" value=""><?php esc_html_e('Choose a Speciality','agrodir'); ?></option>	
							<?php
							$specialtie =__('Accepts Apple Pay, Accepts Credit Cards, Alcohol, Bike Parking, Coupons,Delivery,Good for Kids,Live Music,Pets Friendly, Reservations, Smoking Allowed,Street Parking, Wheelchair Accessible,Wireless Internet
															','agrodir');
																										
											$field_set=get_option('iv_directories_specialitie' );
											if($field_set!=""){ 
													$specialtie=get_option('iv_directories_specialitie' );
											}			
																	
														
										$i=1;		
											
										$specialtie_fields= explode(",",$specialtie);			
										foreach ( $specialtie_fields as $field_value ) { ?>	
												<option  class="cbp-search-select" value="<?php echo $field_value; ?>"><?php echo $field_value; ?></option>
											
										<?php
										}
										?>	
											
						</select>
				  </div>
			  </div>

			  <div class="" >
				  <div class="form-group" >
						<i class="fa fa-chevron-down arrow"></i>
						<select name="dir_type"  id="dir_type" class="cbp-search-select">
						<option  class="cbp-search-select" value="rurl_1"><?php echo ucfirst($directory_url_1_string); ?></option>
						<option class="cbp-search-select"  value="rurl_2"><?php echo ucfirst($directory_url_2_string); ?></option>
						</select>
				  </div>
			  </div>

				<div class="" >
          <div class="form-group search" >
					     <button type="button" id="search_submit_m" name="search_submit_m"  onClick='submitSearchForm()' class="btn-new btn-custom-search "> <i class="fa fa-search"></i> <span><?php esc_html_e( 'Search', 'agrodir' ); ?></span></button>
				  </div>
        </div>
		 </div>
  </div>
</form>

<?php 
 wp_enqueue_script( 'search-form-js', agrodir_JS.'search-form.js', array('jquery'), $ver = true, true );
 wp_localize_script( 'search-form-js', 'jsdata', array( 'cpost1_url' => get_post_type_archive_link( $directory_url_1),'cpost2_url'=> get_post_type_archive_link( $directory_url_2 )) );
 
?>   
 

