<?php

$directory_url_1=get_option('_iv_directory_url_1');					
if($directory_url_1==""){$directory_url_1='producers';}	

$directory_url_2=get_option('_iv_directory_url_2');					
if($directory_url_2==""){$directory_url_2='retailers';}



$title=(isset($atts['cpt1_category_title'])?$atts['cpt1_category_title']:'Producers Categories');
$banner_subtitle=(isset($atts['cpt1_category_sub_title'])?$atts['cpt1_category_sub_title']:'With over 3000 producers offeres across 20 countries agrodir is the right place to find your closest law service provider thal will help you in market');



?>
	
 <div class="blog-content pbzero home-blog">
 		<div class="container-fluid text-center">
<h2 class="home-title" style="text-align: center;"><strong><?php echo $title;?></strong></h2>
<div class="home-subtitle"><?php echo $banner_subtitle;?></div>

<div style="text-align: center;"><?php echo do_shortcode('[producers_categories]')?></div>

</div></div>
